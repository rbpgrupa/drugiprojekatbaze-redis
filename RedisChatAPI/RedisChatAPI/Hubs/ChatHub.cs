﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedisChatAPI.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string korisnik, string poruka)
        {
            await Clients.All.SendAsync("Prijem poruke", korisnik, poruka);
        }
    }
}
