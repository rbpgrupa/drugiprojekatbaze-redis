﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedisChatAPI
{
    public class RedisDatabase : IRedisDatabase
    {
        public IDatabase BazaPodataka { get; }

        public RedisDatabase()
        {
            this.BazaPodataka = ConnectionMultiplexer.Connect("localhost").GetDatabase();
        }
    }
}
