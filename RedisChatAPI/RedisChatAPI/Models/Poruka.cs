﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedisChatAPI.Models
{
    public class Poruka
    {//mislim i da mi treba chatRoom naziv, laksi ce da mi bude zivot, a to je poenta uostalom za ovakve stvari
        public string Autor { get; set; }
        public string Sadrzaj { get; set; }

        public Poruka()
        {
            this.Autor = "";
            this.Sadrzaj = "";
        }

        public Poruka(string autorPoruke, string sadrzajPoruke)
        {
            this.Autor = autorPoruke;
            this.Sadrzaj = sadrzajPoruke;
        }
    }
}
