﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedisChatAPI.Models
{
    public class Korisnik //mozda i nece da treba, videcu na sta ce da lici forma sutra, malo sam i umoran, bem ga
    {
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }

        public Korisnik()
        {
            this.KorisnickoIme = "";
            this.Lozinka = "";
        }

        public Korisnik(string korisnickoIme, string lozinka)
        {
            this.KorisnickoIme = korisnickoIme;
            this.Lozinka = lozinka;
        }
    }
}
