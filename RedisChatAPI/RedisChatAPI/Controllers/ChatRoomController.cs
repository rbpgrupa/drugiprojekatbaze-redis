﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RedisChatAPI.Models;
using StackExchange.Redis;

namespace RedisChatAPI.Controllers
{
    [Route("api/[controller]/[action]")] //trebam da menjam URL samo, ovo [action] je naziv metoda
    [ApiController]
    public class ChatRoomController : ControllerBase
    {
        private ConnectionMultiplexer redisKlijent = ConnectionMultiplexer.Connect("localhost");

        //GET: api/Poruka
        [HttpGet]
        public IEnumerable<string> Get()
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            var naziviSobaIzBaze = bazaPodataka.SetMembers($"s_chatRoomNames");
            List<string> naziviSoba = new List<string>();

            foreach (var naziv in naziviSobaIzBaze)
                naziviSoba.Add(naziv.ToString());

            return naziviSoba;
        }

        // GET: api/Poruka/Get/nesto
        [HttpGet("{chatRoom}")]
        public IEnumerable<Poruka> Get(string chatRoom)
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            var listaPoruka = bazaPodataka.ListRange($"l_{chatRoom}");
            List<Poruka> porukeZaNazad = new List<Poruka>();

            foreach (var zapisPoruke in listaPoruka)
            {
                string autorIPoruka = zapisPoruke.ToString();
                string[] odvojeniParametri = autorIPoruka.Split(":");
                porukeZaNazad.Add(new Poruka(odvojeniParametri[0], odvojeniParametri[1]));
            }

            return porukeZaNazad;
        }

        
        [HttpPost]
        public int PostPoruka([FromBody] Dictionary<string, Poruka> chatSobaIPoruka)
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            string nazivChatSobe = chatSobaIPoruka.Keys.ElementAt(0);//ovo kao znam da postoji, pa nema provere...
            Poruka poruka = chatSobaIPoruka.Values.ElementAt(0);
            
            bazaPodataka.ListRightPush($"l_{nazivChatSobe}", $"{poruka.Autor}:{poruka.Sadrzaj}");

            return 4;
        }

        // POST: api/novaSoba --->> dodavanje imena chatrooma, ako je slobodno
        [HttpPost]
        public string PostNovaSoba([FromBody] string nazivChatSobe)
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            bool uspesnoDodavanje = bazaPodataka.SetAdd($"s_chatRoomNames", nazivChatSobe);

            if (!uspesnoDodavanje)
                return "1001";//zauzeto ime za sobu
            else
                return "1000";//slobodno ime za sobu
        }

    }
}
