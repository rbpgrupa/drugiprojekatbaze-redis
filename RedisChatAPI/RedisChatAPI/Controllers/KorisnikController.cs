﻿using Microsoft.AspNetCore.Mvc;
using RedisChatAPI.Models;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Net.Http;

namespace RedisChatAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KorisnikController : ControllerBase
    {
        ConnectionMultiplexer redisKlijent = ConnectionMultiplexer.Connect("localhost");
        //private ConnectionMultiplexer redisKlijent = ConnectionMultiplexer.Connect("localhost");
        // GET: api/Korisnik

        [HttpGet]
        public IEnumerable<string> Get()
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            var korisnickaImena = bazaPodataka.SetMembers("s_usernames");
            List<string> listaKorisnickihImena = new List<string>();
            foreach (var korisnickoIme in korisnickaImena)
                listaKorisnickihImena.Add($"korisnickoIme: {korisnickoIme.ToString()}");

            return listaKorisnickihImena;
        }

        // Post: api/Korisnik/korisnickoImeKojeMozdaNeValja, mora POST jer GET ne podrzava body...
        [HttpPost("{korisnik.KorisnickoIme}")]
        public string PostProvera([FromBody]Korisnik korisnik)
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            bool korisnikPostoji = bazaPodataka.SetContains("s_usernames", korisnik.KorisnickoIme);
            if (!korisnikPostoji)
                return "1001";//nepostojeci korisnik
            else
            {
                var lozinkaIzBaze = bazaPodataka.HashGet($"h_{korisnik.KorisnickoIme}", "password");
                if (lozinkaIzBaze.ToString() == korisnik.Lozinka)
                    return "1000";//sve valja
                else
                    return "1002";//neispravna lozinka
            }
        }

        // POST: api/Korisnik
        [HttpPost]
        public Korisnik Post([FromBody] Korisnik noviKorisnik)
        {
            IDatabase bazaPodataka = this.redisKlijent.GetDatabase();
            bool uspesnoDodavanje = bazaPodataka.SetAdd("s_usernames", noviKorisnik.KorisnickoIme);//O(1) vs O(n)
            if (!uspesnoDodavanje)
            {
                return new Korisnik { KorisnickoIme = "", Lozinka = noviKorisnik.Lozinka };//vraca korisnika sa praznim imenom
            }
            else
            {
                bazaPodataka.HashSet($"h_{noviKorisnik.KorisnickoIme}",
                                     new HashEntry[] { new HashEntry("username", noviKorisnik.KorisnickoIme), 
                                                       new HashEntry("password", noviKorisnik.Lozinka) 
                                                     }
                                     );
                return noviKorisnik;
            }
        }

        // PUT: api/Korisnik/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
