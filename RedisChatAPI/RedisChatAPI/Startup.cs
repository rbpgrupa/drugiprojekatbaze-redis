using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RedisChatAPI.Hubs;
using StackExchange.Redis.Extensions.Core.Abstractions;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Core.Implementations;

namespace RedisChatAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            var redisKonfiguracija = this.Configuration.GetSection("Redis").Get<RedisConfiguration>();
            services.AddSingleton(redisKonfiguracija);
            //services.AddScoped<IRedisDatabase, RedisDatabase>();
           
            services.AddCors(options => {
                options.AddPolicy("MyCorsPolicy",
                    builder => builder.SetIsOriginAllowedToAllowWildcardSubdomains()
                                      .WithOrigins("http://localhost:3000")
                                      .AllowAnyMethod()
                                      .AllowCredentials()
                                      .AllowAnyHeader()
                                      .Build()
                    );
            });

            services.AddSignalR();

            services.AddControllers();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseCors("MyCorsPolicy");//ovo se kao treba doda

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/chatHub");//ne kapiram skroz koja je fora sa ovim, al ovo klijent posle hvata kad uspostavlja konekciju, nesto na tu foru
            });
        }
    }
}
