import React from 'react';
import ChatSoba from './komponente/ChatSoba';
import ListaSaSobama from './komponente/ListaSaSobama';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './komponente/Login';
import Registracija from './komponente/Registracija';
import NavigacionaTraka from './komponente/NavigacionaTraka';
import HomePage from './komponente/Home';
import Sponzori from './komponente/Sponzori';

//svaki Link (mora "a" element, nece Link van Router-a) ovde ce da se nalazi i u komponenti NavigacionaTraka
const App: React.FC = () => {  
  return(
    <React.Fragment>
      <BrowserRouter>
      <NavigacionaTraka />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/prijava" component={Login} />
          <Route exact path="/registracija" component={Registracija} />
          <Route exact path="/chatSoba" component={ListaSaSobama} />
          <Route exact path="/chatSoba/:nazivSobe" component={ChatSoba} />
          <Route exact path="/sponzori" component={Sponzori}/>
        </Switch>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
