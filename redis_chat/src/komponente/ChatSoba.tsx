import React from 'react';
import ListaPoruka from './ListaPoruka';
import PoljeZaUnos from './PoljeZaUnos';
import { Poruka } from '../store/poruka/model';
import { NavLink, RouteComponentProps, Redirect } from 'react-router-dom';
import { Dispatch } from 'redux';
import { UcitajPorukeIzBaze } from '../store/poruka/akcije';
import { connect } from 'react-redux';
import { IzadjiIzSobe, UdjiUSobu } from '../store/korisnik/akcije';
import { RootStanje } from '../store';

//ovo ima naziv, svoje poruke, i negde da ubacim back dugme, bem ga
interface Props extends RouteComponentProps<{nazivSobe: string}>
{
    naziv: string,
    poruke: Poruka[],
    loading: boolean,
    nekoJePrijavljen: boolean
}

interface StoreProps
{
    ucitajPorukeZaSobu: Function,
    registrujUlazak: Function,
    izadjiIzSobe: Function
}

type KompletanProps = Props & StoreProps;

class ChatSoba extends React.Component<KompletanProps, {}>
{
    componentDidMount(): void
    {
        this.props.ucitajPorukeZaSobu(`${this.props.match.params.nazivSobe}`);
        this.props.registrujUlazak(this.props.match.params.nazivSobe);
    }

    render(): JSX.Element
    {   
        if(!this.props.nekoJePrijavljen)
            return <Redirect to="/" />

        return(
            <div className="col-sm-6 offset-sm-3 text-center">
                <h1>{ this.props.match.params.nazivSobe }</h1>
                {
                    this.props.loading? this.renderLoadingScreen() : this.renderActiveScreen()
                }
                <PoljeZaUnos />
                <NavLink to="/chatSoba"> <h1 className="col-sm-6 offset-sm-3 text-center">Nazad</h1></NavLink>
            </div>
        );
    }
    
    renderLoadingScreen = (): JSX.Element => {
        return( <h1 className="col-sm-6 offset-sm-3 text-center">LOADING PORUKE, VEIT...</h1>)
    }

    renderActiveScreen = (): JSX.Element => {
        return( <ListaPoruka poruke={this.props.poruke} />);
    }
}

const mapStateToProps = (rootReducer: RootStanje) => {
    const { svePoruke, korisnikDetalji } = rootReducer;
    return {
        poruke: svePoruke.poruke,
        nekoJePrijavljen: korisnikDetalji.korisnikJeUlogovan,
        loading: svePoruke.loading,
    }
}

const mapDispatchToProps = (dispatch: Dispatch): StoreProps => {
    return {
        ucitajPorukeZaSobu: (nazivSobe: string) => dispatch(UcitajPorukeIzBaze(nazivSobe)),
        registrujUlazak: (nazivSobe: string) => dispatch(UdjiUSobu(nazivSobe)),
        izadjiIzSobe: () => dispatch(IzadjiIzSobe())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatSoba);