import React from 'react';
import { Poruka } from '../store/poruka/model';
import PorukaKomponenta from './PorukaKomponenta';

interface Props
{
    poruke: Poruka[]
}

//odavde trebam da pomerim redux u komponentu iznad (ChatSoba) koja treba da dobije poruke za chat sobu
//i onda da pusti ovoj komponenti kao prop to da se iscrta
class ListaPoruka extends React.Component<Props, {}>
{
    render(): JSX.Element
    {
        return (
            <React.Fragment>
                <div>
                    {this.props.poruke.map((poruka: Poruka, redniBrojPoruke: number) => {
                            return <PorukaKomponenta key={redniBrojPoruke}
                                                     poruka={poruka}/>
                        })
                    }
                </div>
            </React.Fragment>
        );
    }


}

export default ListaPoruka;