import React from 'react';

import blackwater from '../slike/sponzoriSlike/blackwater.png';
import cia from '../slike/sponzoriSlike/cia.png'
import mi6 from '../slike/sponzoriSlike/mi6.jpg'
import mosad from '../slike/sponzoriSlike/mosad.png'
import djilas from '../slike/sponzoriSlike/djilas.jpg'
import miskovic from '../slike/sponzoriSlike/miskovic.jpg'
import soros from '../slike/sponzoriSlike/soros.jpg'


class Sponzori extends React.Component<{}, {}>
{
    render(): JSX.Element
    {
        return(
            <div className="col-sm-6 offset-sm-2 text-center">
                <h1>Naši generalni sponzori</h1>
                <img src={blackwater} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={cia} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={mi6} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={mosad} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={djilas} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={miskovic} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={soros} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
            </div>
        );
    }
}

export default Sponzori;