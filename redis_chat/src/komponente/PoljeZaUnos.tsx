import React from 'react';
import { Dispatch } from 'redux';
import { Poruka } from '../store/poruka/model';
import { UpisiPorukuUBazu } from '../store/poruka/akcije';
import { connect } from 'react-redux';
import { RootStanje } from '../store';

interface Stanje
{
    tekstPoruke: string
}

interface Props
{
    nekoJePrijavljen: boolean,
    usernamePrijavljenog: string,
    aktivnaSoba: string
}

interface ActionProps
{
    posaljiPoruku: (novaPoruka: Poruka) => void
}

//------->>>> ovde trebam da dodam iz koje je sobe poslata poruka, i ko je autor poruke
class PoljeZaUnos extends React.Component<Props & ActionProps, Stanje>
{
    readonly state = {
        tekstPoruke: ''
    };

    render(): JSX.Element
    {
        return(
            <div>
                <hr/>
                <form id="idFormeZaUnos" onSubmit={this.handleKlika}>
                    <div>
                        Unesi poruku: <textarea id="textarea"
                                                onChange={this.setujPoruku}
                                                onKeyDown={this.handleUnosaPoruke}
                                                rows={4}
                                                cols={30}>
                                      </textarea>
                    </div>
                    <button type="submit"
                            className="btn btn-warning btn-lg"
                    >Pošalji poruku</button>
                </form>
            </div>
        );
    }

    setujPoruku = (event: React.ChangeEvent<HTMLTextAreaElement>): void => {
        this.setState({tekstPoruke: event.target.value});
    }

    handleUnosaPoruke = (event: React.KeyboardEvent<HTMLTextAreaElement>): void => {
        let textarea: HTMLTextAreaElement = (document.getElementById("textarea")) as HTMLTextAreaElement;
        
        if(event.key === 'Enter' && event.shiftKey === false)
        {
            let novaPoruka: Poruka = this.izvuciPorukuIzKomponente();
            
            this.props.posaljiPoruku(novaPoruka);
            this.setState({tekstPoruke: ''});
            textarea.value = "";
        }
        
    }

    handleKlika = (event: React.FormEvent<HTMLFormElement>): void => {
        event.preventDefault();

        let novaPoruka: Poruka = this.izvuciPorukuIzKomponente();
        this.props.posaljiPoruku(novaPoruka);
    }

    izvuciPorukuIzKomponente = (): Poruka => {
        return {
            nazivSobe: this.props.aktivnaSoba,
            autor: this.props.usernamePrijavljenog,
            sadrzaj: this.state.tekstPoruke
        }
    }
}

const mapStateToProps = (reducer: RootStanje): Props => {
    const { korisnikDetalji } = reducer;
    return {    
        nekoJePrijavljen: korisnikDetalji.korisnikJeUlogovan,
        usernamePrijavljenog: korisnikDetalji.prijavljeniKorisnik.korisnickoIme,
        aktivnaSoba: korisnikDetalji.aktivnaChatSoba
    }
}

const mapDispatchToProps = (dispatch: Dispatch): ActionProps => {
    return {
        posaljiPoruku: (novaPoruka: Poruka) => dispatch(UpisiPorukuUBazu(novaPoruka))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PoljeZaUnos);