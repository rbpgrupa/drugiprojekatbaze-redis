import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Korisnik } from "../store/korisnik/model";
import { Dispatch } from "redux";
import { PrijaviKorisnika } from "../store/korisnik/akcije";
import { RootStanje } from "../store";

interface State
{
    korisnickoIme: string,
    lozinka: string,
}

interface PropsAkcije
{
    prijaviKorisnika: Function 
}

interface Props
{
    nekoJePrijavljen: boolean,
    pogresnoKorisnickoIme: boolean,
    pogresnaSifra: boolean
}

class Login extends React.Component<Props & PropsAkcije, State>
{
    state = {
        korisnickoIme: '',
        lozinka: ''
    };

    render(): JSX.Element
    {
        if(this.props.nekoJePrijavljen)
            return <Redirect exact to="/chatSoba" />

        return (
            <div className="col-sm-6 offset-sm-3 text-center">
                <div className="form-group">
                    <h3>Prijavi se:</h3>
                    <input type="text" 
                           name="inputKorisnickoIme" 
                           placeholder="Unesi korisničko ime" 
                           className="form-control"
                           onChange={this.onChangeKorisnickoIme}/>
                           {
                               this.props.pogresnoKorisnickoIme && <p style={{ color : "red" }}>Korisničko ime ne postoji</p>
                           }
                    <hr/>
                    <input type="password" 
                           name="inputSifraKorisnika" 
                           placeholder="Unesi šifru"
                           className="form-control" 
                           onChange={this.onChangeLozinka}/>
                           {
                               this.props.pogresnaSifra && <p style={{ color : "red" }}>Pogrešna lozinka</p>
                           }
                    <hr/>
                </div>
                <div className="form-group">
                    <button className="btn btn-primary btn-lg" 
                            onClick={this.handleKlika}>Potvrdi
                    </button>
                </div>
            </div>
        );
    }

    onChangeKorisnickoIme = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ korisnickoIme: event.target.value });
    }

    onChangeLozinka = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ lozinka: event.target.value });
    }

    handleKlika = (event: React.MouseEvent<HTMLButtonElement>): void => {
        let korisnik: Korisnik = {
            korisnickoIme: this.state.korisnickoIme,
            lozinka: this.state.lozinka
        };

        this.props.prijaviKorisnika(korisnik);
    }
}

const mapStateToProps = (reducer: RootStanje): Props => {
    const { korisnikDetalji } = reducer;
    return {
        nekoJePrijavljen: korisnikDetalji.korisnikJeUlogovan,
        pogresnoKorisnickoIme: korisnikDetalji.loseKorisnickoIme,
        pogresnaSifra: korisnikDetalji.losaLozinka,
    }
}

const mapDispatchToProps = (dispatch: Dispatch): PropsAkcije => {
    return {
        prijaviKorisnika: (korisnik: Korisnik) => dispatch(PrijaviKorisnika(korisnik))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);