import React from "react";
import { Dispatch } from "redux";
import { Korisnik } from "../store/korisnik/model";
import { UpisiKorisnika } from "../store/korisnik/akcije";
import { connect } from "react-redux";
import { RouteComponentProps, Redirect } from "react-router-dom";
import { RootStanje } from "../store";

interface State
{
    korisnickoIme: string,
    lozinka: string,
    redirektPotencijalni: boolean
}

interface Props extends RouteComponentProps<any>
{
    napraviNalog: Function,
    zauzetoKorisnickoIme: boolean,
    uspesnoLogovanje: boolean
}

class Registracija extends React.Component<Props, State>
{
    readonly state = {
        korisnickoIme: '',
        lozinka: '',
        redirektPotencijalni: false
    };

    render(): JSX.Element
    {
        if(this.props.uspesnoLogovanje)
        {
            alert(`Uspesno registrovan nalog`);
            return <Redirect exact to="/ChatSoba"/>
        }
        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3 text-center">
                    <h3>Napravi nalog</h3>

                    <div className="form-group">
                        <label className="control-label">Korisničko ime:</label>
                        <input type="text" 
                               name="inputKorisnickoIme" 
                               className="form-control" 
                               onChange={this.onChangeKorisnickoIme}/>
                        {
                            this.props.zauzetoKorisnickoIme && <p style={{ color : "red" }}>Korisničko ime je zauzeto</p>
                        }
                        <label className="control-label">Lozinka:</label>
                        <input type="password" 
                               name="inputLozinka" 
                               id="" 
                               className="form-control" 
                               onChange={this.onChangeLozinka}/>
                    </div>
                    
                    <div className="form-group">
                        <button className="btn btn-primary btn-lg" 
                                onClick={this.napraviNalog}>
                            Napravi nalog
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    onChangeKorisnickoIme = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ korisnickoIme: event.target.value });
    }

    onChangeLozinka = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ lozinka: event.target.value });
    }

     napraviNalog = (event: React.MouseEvent<HTMLButtonElement>): void => {
        let korisnik: Korisnik = {
            korisnickoIme: this.state.korisnickoIme,
            lozinka: this.state.lozinka
        };

        this.props.napraviNalog(korisnik);
    }
}

const mapStateToProps = (reducer: RootStanje) => {
    const { korisnikDetalji } = reducer;
    return {
        zauzetoKorisnickoIme: korisnikDetalji.loseKorisnickoIme,
        uspesnoLogovanje: korisnikDetalji.korisnikJeUlogovan
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        napraviNalog: (korisnik: Korisnik) => dispatch(UpisiKorisnika(korisnik))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registracija);