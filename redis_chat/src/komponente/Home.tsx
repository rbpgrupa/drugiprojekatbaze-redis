import React from "react";
import redis from '../slike/tehnologijeSlike/redis logo.png';
import dotnet from '../slike/tehnologijeSlike/dotnet core.png'
import react from '../slike/tehnologijeSlike/react.png'
import redux from '../slike/tehnologijeSlike/redux-logo-landscape.png'

class HomePage extends React.Component<{}, {}>
{
    render(): JSX.Element
    {
        return(
            <div className="col-sm-6 offset-sm-2 text-center">
                <h1>Dobrodošli na Redis Chat</h1>
                <img src={redis} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={dotnet} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={react} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
                <img src={redux} alt="Nema slike" className="col-sm-12 offset-sm-3 text-center"/>
            </div>
        );
    }
}

export default HomePage;