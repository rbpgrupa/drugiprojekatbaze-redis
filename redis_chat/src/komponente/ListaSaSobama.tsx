import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { UcitajNaziveSoba, DodavanjeNoveSobe } from '../store/chatSobe/akcije';
import { RootStanje } from '../store';


export interface StoreProps
{
    ucitajNaziveSoba: () => void,
    dodajNovuSobu: (nazivSobe: string) => void
}

export interface Props
{
    naziviSoba: string[], //samo naziv, sama ChatSoba ima listu svojih poruka i ono polje za unos
    nekoJePrijavljen: boolean,
    loading: boolean,
    zauzetNazivSobe: boolean
}

export interface State
{
    nazivNoveSobe: string
}

class ListaSaSobama extends React.Component<Props & StoreProps, State>
{
    componentDidMount(): void
    {
        if(this.props.naziviSoba.length === 0)
            this.props.ucitajNaziveSoba();
    }

    render(): JSX.Element
    {
        if(!this.props.nekoJePrijavljen)
            return <Redirect exact to="/" />

        return(
            <React.Fragment> 
                <h1 className="col-sm-6 offset-sm-3 text-center">Raspoložive chat sobe</h1>
                <div className="col-sm-6 offset-sm-3 text-center">
                        <input placeholder="Dodaj novu sobu" 
                               onChange={this.handleUnosaNoveSobe}
                               className="form-control"/>
                        <button onClick={this.dodajNovuSobu}
                                className="btn btn-outline-success">Dodaj novu sobu</button>
                        {
                            !this.props.zauzetNazivSobe && <p style={{ color : "red" }}>Naziv sobe je zauzet</p>
                        }
                </div>
                    {
                        (this.props.naziviSoba.length === 0)? this.prikaziLoading() : this.prikaziSobe()                             
                    }
            </React.Fragment>
        );
    }

    prikaziLoading = (): JSX.Element => {
        return (<React.Fragment><h1 className="col-sm-6 offset-sm-3 text-center">LOADING...</h1></React.Fragment>);
    }

    prikaziSobe = (): JSX.Element[] => {
        return (this.props.naziviSoba.map((nazivSobe:string, redniBr: number) => {
            return (
                <React.Fragment key={redniBr}>
                    <Link to={`/chatSoba/${nazivSobe}`}
                          className="col-sm-6 offset-sm-3 text-center">
                        <h4> {nazivSobe} </h4>
                    </Link>
                    <hr/>
                </React.Fragment>
            );
        })
        );
    }

    handleUnosaNoveSobe = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({nazivNoveSobe : event.target.value });
    }

    dodajNovuSobu = (): void => {
        this.props.dodajNovuSobu(this.state.nazivNoveSobe);
    }

}

const mapStateToProps = (rootReducer: RootStanje): Props => {
    const { naziviSoba, korisnikDetalji } = rootReducer;
    return {
        naziviSoba: naziviSoba.naziviSoba,
        loading: naziviSoba.loading,
        nekoJePrijavljen: korisnikDetalji.prijavljeniKorisnik.korisnickoIme? true : false,
        zauzetNazivSobe: naziviSoba.nazivSobeJeIspravan
    }
}

const mapDispatchToProps = (dispatch: Dispatch): StoreProps => {
    return {
        ucitajNaziveSoba: () => dispatch(UcitajNaziveSoba()),
        dodajNovuSobu: (nazivSobe: string) => dispatch(DodavanjeNoveSobe(nazivSobe))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ListaSaSobama);