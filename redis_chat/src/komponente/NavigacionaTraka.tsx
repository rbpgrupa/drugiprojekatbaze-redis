import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { OdjaviKorisnika } from "../store/korisnik/akcije";
import { RootStanje } from "../store";

interface Props
{
    nekoJePrijavljen: boolean,
    usernamePrijavljenog: string
}

interface ActionProps
{
    odjavaKorisnika: Function
}

class NavigacionaTraka extends React.Component<Props & ActionProps, any>
{
    render(): JSX.Element
    {
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <h4>RedisChat - Dobar kao burek posle pijanke</h4>
                    <div className="navbar-header">
                        <NavLink exact to="/" 
                            className="navbar-brand"
                            activeClassName="btn btn-outline-warning">
                            <h3>Home</h3>
                        </NavLink>
                        {this.props.nekoJePrijavljen? this.renderZaUlogovane() : this.renderZaIzlogovane() }
                        <NavLink exact to="/sponzori" 
                            className="navbar-brand"
                            activeClassName="btn btn-outline-warning">
                            <h3>Sponzori</h3>
                        </NavLink>
                    </div>
                </div>
            </nav>
        )
    }

    renderZaUlogovane(): JSX.Element
    {
        return(
            <React.Fragment>
                <NavLink exact to="/chatSoba" 
                         className="navbar-brand"
                         activeClassName="btn btn-outline-warning">
                         <h3>Chat sobe</h3>
                </NavLink>
                <a href="/" onClick={this.odjaviKorisnika} 
                         className="navbar-brand"
                         >
                         <h3>({ 
                            this.props.usernamePrijavljenog? <span style={{color: 'orange'}}> {this.props.usernamePrijavljenog} </span> : <React.Fragment/>
                            }): Izloguj se
                         </h3>
                </a>
        
            </React.Fragment>
        );
    }

    renderZaIzlogovane(): JSX.Element
    {
        return(
            <React.Fragment>
                <NavLink exact to="/prijava" 
                         className="navbar-brand" 
                         activeClassName="btn btn-outline-warning">
                         <h3>Prijava</h3>
                </NavLink>                    
                <NavLink exact to="/registracija" 
                         className="navbar-brand"
                         activeClassName="btn btn-outline-warning">
                         <h3>Registracija</h3>
                </NavLink>
            </React.Fragment>
        );
    }

    odjaviKorisnika = () => {

    }
}

const mapStateToProps = (reducer: RootStanje): Props => {
    const { korisnikDetalji } = reducer;
    return {
        nekoJePrijavljen: korisnikDetalji.korisnikJeUlogovan,
        usernamePrijavljenog: korisnikDetalji.prijavljeniKorisnik.korisnickoIme
    }
}

const mapDispatchToProps = (dispatch: Dispatch):ActionProps => {
    return {
        odjavaKorisnika: () => dispatch(OdjaviKorisnika())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigacionaTraka);