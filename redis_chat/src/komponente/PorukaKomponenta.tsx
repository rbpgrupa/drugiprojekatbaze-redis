import React from 'react';
import { Poruka } from '../store/poruka/model';

interface Props
{
    poruka: Poruka
}

class PorukaKomponenta extends React.Component<Props>
{
    render(): JSX.Element
    {
        const { autor, sadrzaj } = this.props.poruka;
        return(
            <div>
                <h4> { autor } </h4>
                <p>  { sadrzaj } </p>
            </div>
        );
    }

}

export default PorukaKomponenta;