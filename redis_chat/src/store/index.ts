import { combineReducers, createStore, applyMiddleware } from "redux";
import { reducerPoruka, StanjeSvihPoruka } from "./poruka/reducerPoruka";
import { reducerChatSoba, StanjeChatSoba } from "./chatSobe/reducerChatSoba";
import { reducerKorisnika, KorisnickoStanje } from "./korisnik/reducerKorisnika";

import { composeWithDevTools } from 'redux-devtools-extension';
import { all } from "redux-saga/effects";
import createSagaMiddleware, { SagaMiddleware } from '@redux-saga/core';
import { rootSagaZaPoruke } from "./sage/sagaPoruka";
import { rootSagaZaChatSobe } from "./sage/sagaChatSobe";
import { rootSagaZaKorisnike } from "./sage/sagaKorisnik";

export interface RootStanje //<<--- ne mogu lepo da namestim rootStanje, al mogu u mapDispatchToProps kao argument...
{
    svePoruke: StanjeSvihPoruka,
    naziviSoba: StanjeChatSoba,
    korisnikDetalji: KorisnickoStanje
}


const rootReducer = combineReducers({
    svePoruke: reducerPoruka,
    naziviSoba: reducerChatSoba,
    korisnikDetalji: reducerKorisnika
});

function* rootSaga()
{
    yield all([
        rootSagaZaPoruke(),
        rootSagaZaChatSobe(),
        rootSagaZaKorisnike()
    ]);
}

export default function konfigurisiStore()
{
    const sagaMiddleware: SagaMiddleware = createSagaMiddleware();
    const store = createStore(
        rootReducer,
        composeWithDevTools(applyMiddleware(sagaMiddleware))
    );
    sagaMiddleware.run(rootSaga);

    return store;
}