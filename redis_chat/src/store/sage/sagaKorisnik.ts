import * as saga from 'redux-saga/effects';
import { AkcijeKorisnik, UpisivanjeKorisnika, PrijavljivanjeKorisnika } from '../korisnik/model';
import { ProslediKorisnikaUspeh, ProslediKorisnikaGreska, PogresnaLozinka, PogresnoKorisnickoIme } from '../korisnik/akcije';

const kontrolerZaKorisnike: string = "https://localhost:44351/api/Korisnik";

export function* rootSagaZaKorisnike()
{
    yield saga.all([saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve()
{
    yield saga.takeEvery(AkcijeKorisnik.UPISI_KORISNIKA_U_BAZU, pokusajRegistracije);
    yield saga.takeEvery(AkcijeKorisnik.PRIJAVI_KORISNIKA, pokusajPrijave);
}

function* pokusajPrijave(akcija: PrijavljivanjeKorisnika)
{
    const { korisnik } = akcija;
    //nisam slao nista u bazu jer sam konj.... mora POST jer GET ne podrzava body
    let statusKodPrijave = yield uputiZahtevKaBazi("POST", 
                                                       `${kontrolerZaKorisnike}/${korisnik.korisnickoIme}`, 
                                                        korisnik);
    
    switch(statusKodPrijave)
    {
        case 1001: yield saga.put(PogresnoKorisnickoIme()); break;
        
        case 1002: yield saga.put(PogresnaLozinka()); break;
         
        default: yield saga.put(ProslediKorisnikaUspeh(korisnik)); break;
         
    }

}

function* pokusajRegistracije(akcija: UpisivanjeKorisnika)
{
    let { noviKorisnik } = akcija;
    
    //--->> razlika od prijave iznad je ta sto se lozinka NE PROVERAVA
    const rezultatIzBaze = yield uputiZahtevKaBazi("POST", 
                                                   kontrolerZaKorisnike, 
                                                   noviKorisnik);
    
    if(rezultatIzBaze.korisnickoIme === "")
        yield saga.put(ProslediKorisnikaGreska(rezultatIzBaze));
    else
        yield saga.put(ProslediKorisnikaUspeh(rezultatIzBaze));
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)//podaci su opcioni, valjda ovako ide
{
    let podaciZaPost: RequestInit = {
        body: JSON.stringify(podaci),
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, */*'
        }
    };

    let podaciZaGet: RequestInit = {
        method: metoda,
        headers: 
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, */*'
        }
    };

    let ishodFetcha = podaci? yield fetch(URL, podaciZaPost) : yield fetch(URL, podaciZaGet);
    
    if(ishodFetcha.ok)
        return yield ishodFetcha.json();
    else
        return ishodFetcha.status.toString();
}