import * as saga from 'redux-saga/effects';
import { AkcijePoruke, UpisivanjePoruke, UcitavanjePoruka } from '../poruka/model';
import { ProslediPorukeReduceru } from './sagaAkcije';

const kontrolerZaPoruke: string = "https://localhost:44351/api/ChatRoom";

export function* rootSagaZaPoruke()
{
    yield saga.all([saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve()
{
    yield saga.takeEvery(AkcijePoruke.UCITAJ_PORUKE_IZ_BAZE, ucitajIzBaze);
    yield saga.takeEvery(AkcijePoruke.UPISI_PORUKU_U_BAZU, upisiUBazu);
}

function* ucitajIzBaze(akcija: UcitavanjePoruka)
{
    const { nazivSobe } = akcija;
    let poruke = yield uputiZahtevKaBazi("GET", `${kontrolerZaPoruke}/Get/${nazivSobe}`, null);
    
    yield saga.put(ProslediPorukeReduceru(poruke));
}

function* upisiUBazu(akcija: UpisivanjePoruke)
{
    //da vidim dal sam resio hardkodovanje sobe...
    let { poruka } = akcija;
    console.log(poruka);
    let chatRoomObjekat = {
        [poruka.nazivSobe]: { 
            "autor": poruka.autor,
            "sadrzaj": poruka.sadrzaj
        }
    };

    console.log(chatRoomObjekat);
    yield uputiZahtevKaBazi("POST", `${kontrolerZaPoruke}/PostPoruka`, chatRoomObjekat);
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)//podaci su opcioni, valjda ovako ide
{
    ///<<--- proveriti podatke ovde i onaj stringify
    console.log(JSON.stringify(podaci));
    let podaciZaPost: RequestInit = {
        body: JSON.stringify(podaci),
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, */*'
        }
    };

    let podaciZaGet: RequestInit = {
        method: metoda,
        headers: 
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, */*'
        }
    };

    let ishodFetcha = podaci? yield fetch(URL, podaciZaPost) : yield fetch(URL, podaciZaGet);
    
    console.log(ishodFetcha);

    if(ishodFetcha.ok && podaciZaPost)
    {
        console.log(ishodFetcha);
        return yield ishodFetcha.json();
    }
    else
        return ishodFetcha.status.toString();
}