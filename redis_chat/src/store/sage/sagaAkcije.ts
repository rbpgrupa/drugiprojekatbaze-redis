import { Poruka, AkcijePoruke, ProsledjivanjePoruka } from "../poruka/model"
import { ProslediReduceru, AkcijeChatSobe } from "../chatSobe/model"

export const ProslediPorukeReduceru = (ucitanePoruke: Poruka[]): ProsledjivanjePoruka => {
    return {
        type: AkcijePoruke.PROSLEDI_PORUKE_REDUCERU,
        poruke: ucitanePoruke
    }
}

export const ProslediNaziveReduceru = (nazivi: string[]): ProslediReduceru => {
    return {
        type: AkcijeChatSobe.PROSLEDI_NAZIVE_REDUCERU,
        nazivi: nazivi
    }
}