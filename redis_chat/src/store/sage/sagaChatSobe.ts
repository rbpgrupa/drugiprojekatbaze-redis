import * as saga from 'redux-saga/effects';
import { AkcijeChatSobe, DodajNovuSobu } from '../chatSobe/model';
import { ProslediNaziveReduceru } from './sagaAkcije';
import { DodajSobuGreska, DodajSobuUspeh } from '../chatSobe/akcije';

//nakon vecere moram da apdejtujem URL-ove
const kontrolerZaSobu: string = "https://localhost:44351/api/ChatRoom";

export function* rootSagaZaChatSobe()
{
    yield saga.all([saga.fork(posmatrajZahteve)]);
}

function* posmatrajZahteve()
{
    yield saga.takeEvery(AkcijeChatSobe.UCITAJ_NAZIVE_SOBA, ucitajIzBaze);
    yield saga.takeEvery(AkcijeChatSobe.UPISI_SOBU_U_BAZU, upisiUBazu);
}

function* ucitajIzBaze()
{
    let nazivi = yield uputiZahtevKaBazi("GET", `${kontrolerZaSobu}/Get`, null);

    yield saga.put(ProslediNaziveReduceru(nazivi));
}

function* upisiUBazu(akcija: DodajNovuSobu)
{
    let { naziv } = akcija;
    
    let statusKodDodavanja: number = yield uputiZahtevKaBazi("POST", `${kontrolerZaSobu}/PostNovaSoba`, naziv);

    console.log(statusKodDodavanja);
    console.log(statusKodDodavanja === 1001);

    if(statusKodDodavanja === 1001)
        yield saga.put(DodajSobuGreska());
    else
        yield saga.put(DodajSobuUspeh(naziv));
}

function* uputiZahtevKaBazi(metoda: string, URL: string, podaci?: any)
{
    let podaciZaPost: RequestInit = {
        body: JSON.stringify(podaci),
        method: metoda,
        headers:
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, */*'
        }
    };

    let podaciZaGet: RequestInit = {
        method: metoda,
        headers: 
        {
            'Content-Type': 'application/json',
            'mode': 'no-cors',
            'Accept': 'application/json, text/plain, */*'
        }
    };

    let ishodFetcha = podaci? yield fetch(URL, podaciZaPost) : yield fetch(URL, podaciZaGet);
    if(ishodFetcha.ok)
        return yield ishodFetcha.json();
    else
        return ishodFetcha.status.toString();
}