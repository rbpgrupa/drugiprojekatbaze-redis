export interface Korisnik
{
    korisnickoIme: string,
    lozinka: string
}

//1. kad korisnik napravi novi nalog
//2. kad korisnik unese postojeci nalog i hoce da se prijavi
export enum AkcijeKorisnik
{
    UPISI_KORISNIKA_U_BAZU = "[Akcije Korisnik] Upisi korisnika u bazu",
    PRIJAVI_KORISNIKA = "[Akcije Korisnik] Prijavi korisnika",
    PROSLEDI_KORISNIKA_REDUCERU_USPEH = "[Akcije Korisnik] Prosledi korisnika reduceru uspeh",
    PROSLEDI_KORISNIKA_REDUCERU_GRESKA = "[Akcije Korisnik] Prosledi korisnika reduceru greska",
    ODJAVI_KORISNIKA = "[Akcije Korisnik] Odjavi korisnika",
    UDJI_U_SOBU = "[Akcije Korisnik] Udji u sobu",
    IZADJI_IZ_SOBE = "[Akcije Korisnik] Izadji iz sobe",
    POGRESNO_KORISNICKO_IME = "[Akcije Korisnik] Pogresno korisnicko ime",
    POGRESNA_LOZINKA = "[Akcije Korisnik] Pogresna lozinka"
}

export interface UlazakUSobu
{
    type: AkcijeKorisnik,
    nazivSobe: string
}

export interface IzlazakIzSobe
{
    type: AkcijeKorisnik
}

export interface OdjavaKorisnika
{
    type: AkcijeKorisnik
}

export interface UpisivanjeKorisnika
{
    type: AkcijeKorisnik,
    noviKorisnik: Korisnik
}

export interface PrijavljivanjeKorisnika
{
    type: AkcijeKorisnik,
    korisnik: Korisnik
}

export interface UcitavanjeKorisnika
{
    type: AkcijeKorisnik,
    korisnik: Korisnik
}

export interface ProsledjivanjeKorisnika
{
    type: AkcijeKorisnik,
    korisnik: Korisnik
}

export interface PogresanUsername
{
    type: AkcijeKorisnik
}

export interface PogresnaSifra
{
    type: AkcijeKorisnik
}