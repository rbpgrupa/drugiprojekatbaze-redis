import { Action } from "redux";
import { Korisnik, AkcijeKorisnik, ProsledjivanjeKorisnika, UlazakUSobu } from "./model";

export interface KorisnickoStanje
{
    prijavljeniKorisnik: Korisnik,
    aktivnaChatSoba: string,
    loseKorisnickoIme: boolean,
    losaLozinka: boolean,
    korisnikJeUlogovan: boolean
}

const pocetnoStanje: KorisnickoStanje = {
    prijavljeniKorisnik: {
        korisnickoIme: '',
        lozinka: ''
    },
    aktivnaChatSoba: '',
    loseKorisnickoIme: false,
    losaLozinka: false,
    korisnikJeUlogovan: false
};

export function reducerKorisnika(stanje: KorisnickoStanje = pocetnoStanje, akcija: Action<any>): KorisnickoStanje
{
    switch(akcija.type)
    {
        case AkcijeKorisnik.PROSLEDI_KORISNIKA_REDUCERU_GRESKA:
        {
            return {
                ...stanje,
                loseKorisnickoIme: true,
                korisnikJeUlogovan: false
            }
        }

        case AkcijeKorisnik.PROSLEDI_KORISNIKA_REDUCERU_USPEH:
        {
            const { korisnik } = akcija as ProsledjivanjeKorisnika;

            return {
                ...stanje,
                prijavljeniKorisnik: korisnik,
                loseKorisnickoIme: false,
                losaLozinka: false,
                korisnikJeUlogovan: true
            }
        }

        case AkcijeKorisnik.PRIJAVI_KORISNIKA: //ovo ne valja...
        {
            /*const { korisnik } = akcija as ProsledjivanjeKorisnika;

            return {
                ...stanje,
                prijavljeniKorisnik: korisnik,
                greskaKorisnickoIme: false,
                greskaLozinka: false,
                korisnikJeUlogovan: true
            }*/
        }

        case AkcijeKorisnik.ODJAVI_KORISNIKA:
        {               
            return {
                ...stanje,
                prijavljeniKorisnik: {korisnickoIme:"", lozinka:""},
                korisnikJeUlogovan: false
            }
        }

        case AkcijeKorisnik.UDJI_U_SOBU:
        {
            const { nazivSobe } = akcija as UlazakUSobu;

            return {
                ...stanje,
                aktivnaChatSoba: nazivSobe
            }
        }

        case AkcijeKorisnik.IZADJI_IZ_SOBE:
        {
            return {
                ...stanje,
                aktivnaChatSoba: ""
            }
        }

        case AkcijeKorisnik.POGRESNO_KORISNICKO_IME:
        {
            return {
                ...stanje,
                loseKorisnickoIme: true
            }
        }

        //ove dve stvari su medjusobno iskljucive... (ovo i ono sa pogresnim korisnickim imenom)
        case AkcijeKorisnik.POGRESNA_LOZINKA:
        {
            return {
                ...stanje,
                loseKorisnickoIme: false,
                losaLozinka: true
            }
        }

        default:
            return stanje;
    }
}