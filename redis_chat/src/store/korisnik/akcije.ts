import { UpisivanjeKorisnika, AkcijeKorisnik, Korisnik, ProsledjivanjeKorisnika, OdjavaKorisnika, UlazakUSobu, IzlazakIzSobe, PrijavljivanjeKorisnika, PogresnaSifra, PogresanUsername } from "./model";

export const UdjiUSobu = (nazivSobe: string): UlazakUSobu => {
    return {
        type: AkcijeKorisnik.UDJI_U_SOBU,
        nazivSobe: nazivSobe
    }
}

export const IzadjiIzSobe = (): IzlazakIzSobe => {
    return {
        type: AkcijeKorisnik.IZADJI_IZ_SOBE
    }
}

export const UpisiKorisnika = (noviKorisnik: Korisnik): UpisivanjeKorisnika => {
    return {
        type: AkcijeKorisnik.UPISI_KORISNIKA_U_BAZU,
        noviKorisnik: noviKorisnik
    }
}

export const PrijaviKorisnika = (korisnik: Korisnik): PrijavljivanjeKorisnika => {
    return {
        type: AkcijeKorisnik.PRIJAVI_KORISNIKA,
        korisnik: korisnik
    }
}

export const OdjaviKorisnika = (): OdjavaKorisnika => {
    return {
        type: AkcijeKorisnik.ODJAVI_KORISNIKA
    }
}

export const ProslediKorisnikaUspeh = (korisnik: Korisnik): ProsledjivanjeKorisnika => {
    return {
        type: AkcijeKorisnik.PROSLEDI_KORISNIKA_REDUCERU_USPEH,
        korisnik: korisnik
    }
}

export const ProslediKorisnikaGreska = (korisnik: Korisnik): ProsledjivanjeKorisnika => {
    return {
        type: AkcijeKorisnik.PROSLEDI_KORISNIKA_REDUCERU_GRESKA,
        korisnik: korisnik
    }
}

export const PogresnoKorisnickoIme = (): PogresanUsername => {
    return {
        type: AkcijeKorisnik.POGRESNO_KORISNICKO_IME
    }
}

export const PogresnaLozinka = (): PogresnaSifra => {
    return {
        type: AkcijeKorisnik.POGRESNA_LOZINKA
    }
}