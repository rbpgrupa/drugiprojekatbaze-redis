export interface Poruka
{
    nazivSobe: string,
    autor: string,
    sadrzaj: string
}

export interface SkupPoruka
{
    svePoruke: Poruka[]
}

export enum AkcijePoruke
{
    UCITAJ_PORUKE_IZ_BAZE = "[Akcije Poruke] Ucitaj poruke iz baze",
    UPISI_PORUKU_U_BAZU = "[Akcije Poruke] Upisi poruku u bazu",
    PROSLEDI_PORUKE_REDUCERU = "[Akcije Poruke] Prosledi poruke reduceru",
}

//-------->> ovo je sad za akcije
export interface UcitavanjePoruka
{
    type: AkcijePoruke,
    nazivSobe: string
}

export interface ProsledjivanjePoruka
{
    type: AkcijePoruke,
    poruke: Poruka[]
}

export interface UpisivanjePoruke
{
    type: AkcijePoruke,
    poruka: Poruka
}