import { AkcijePoruke, ProsledjivanjePoruka, UpisivanjePoruke, Poruka } from "./model";
import { Action } from "redux";

//ovo trebam da dodam da ovaj reducer vrati
export interface StanjeSvihPoruka {
    poruke: Poruka[],
    loading: boolean
}

const pocetnoStanje: StanjeSvihPoruka = {
    poruke: [],
    loading: true
}

export function reducerPoruka(stanje: StanjeSvihPoruka = pocetnoStanje, akcija: Action): StanjeSvihPoruka
{
    switch(akcija.type)
    {
        case AkcijePoruke.PROSLEDI_PORUKE_REDUCERU:
        {
            const { poruke } = akcija as ProsledjivanjePoruka;
            return {
                ...stanje,
                poruke: [...poruke],
                loading: false
            }
        }

        case AkcijePoruke.UPISI_PORUKU_U_BAZU:
        {
            const { poruka } = akcija as UpisivanjePoruke;
            return {
                ...stanje,
                poruke: [...stanje.poruke, poruka]
            }
        }

        default:
            return stanje;
    }
}