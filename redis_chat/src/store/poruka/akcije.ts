import { UcitavanjePoruka, AkcijePoruke, UpisivanjePoruke, Poruka } from "./model";

//imam dve ovakve  stvari za ucitavanje, jedna upucuje zahtev, druga akcija je ona sto trpa to iz baze tamo negde..
//...gde treba
export const UcitajPorukeIzBaze = (nazivSobe: string): UcitavanjePoruka  => {
    return {
        type: AkcijePoruke.UCITAJ_PORUKE_IZ_BAZE,
        nazivSobe: nazivSobe
    }
}

export const UpisiPorukuUBazu = (novaPoruka: Poruka): UpisivanjePoruke => {
    return {
        type: AkcijePoruke.UPISI_PORUKU_U_BAZU,
        poruka: novaPoruka
    }
}