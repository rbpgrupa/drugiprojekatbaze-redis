import { Action } from "redux";
import { AkcijeChatSobe, ProslediReduceru, DodajNovuSobu, DodavanjeSobeNeuspeh, DodavanjeSobeUspeh } from "./model";
import { AkcijeKorisnik } from "../korisnik/model";

export interface StanjeChatSoba
{
    naziviSoba: string[],
    loading: boolean,
    nazivSobeJeIspravan: boolean
}

const pocetnoStanje: StanjeChatSoba = {
    naziviSoba: [],
    loading: true,
    nazivSobeJeIspravan: true
};

//ovo mora bolje da se uradi..
export function reducerChatSoba(stanje = pocetnoStanje, akcija: Action<any>): StanjeChatSoba
{
    switch (akcija.type)
    {
        case AkcijeChatSobe.PROSLEDI_NAZIVE_REDUCERU:
        {
            const { nazivi } = akcija as ProslediReduceru;
            return {
                ...stanje,
                naziviSoba: nazivi,
                loading: false
            };
        }

        case AkcijeKorisnik.IZADJI_IZ_SOBE:
        {
            return {
                ...stanje,
                loading: true
            }
        }

        case AkcijeChatSobe.DODAVANJE_SOBE_USPEH:
        {
            const { dodavanjeJeUspesno, nazivSobe } = akcija as DodavanjeSobeUspeh;
            return {
                ...stanje,
                nazivSobeJeIspravan: dodavanjeJeUspesno,
                naziviSoba: [...stanje.naziviSoba, nazivSobe]
            }
        }

        case AkcijeChatSobe.DODAVANJE_SOBE_GRESKA:
        {
            const { dodavanjeJeUspesno } = akcija as DodavanjeSobeNeuspeh;
            return {
                ...stanje,
                nazivSobeJeIspravan: dodavanjeJeUspesno
            }
        }

        default:
            return stanje;
    }
}