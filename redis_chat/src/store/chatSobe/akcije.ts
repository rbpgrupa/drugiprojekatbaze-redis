import { AkcijeChatSobe, UcitajSobe, DodajNovuSobu, DodavanjeSobeUspeh, DodavanjeSobeNeuspeh } from "./model";

export const UcitajNaziveSoba = (): UcitajSobe =>{
    return {
        type: AkcijeChatSobe.UCITAJ_NAZIVE_SOBA
    }
}

export const DodavanjeNoveSobe = (nazivSobe: string): DodajNovuSobu => {
    return {
        type: AkcijeChatSobe.UPISI_SOBU_U_BAZU,
        naziv: nazivSobe
    }
}

export const DodajSobuUspeh = (nazivSobe: string): DodavanjeSobeUspeh => {
    return {
        type: AkcijeChatSobe.DODAVANJE_SOBE_USPEH,
        dodavanjeJeUspesno: true,
        nazivSobe: nazivSobe
    }
}

export const DodajSobuGreska = (): DodavanjeSobeNeuspeh => {
    return {
        type: AkcijeChatSobe.DODAVANJE_SOBE_GRESKA,
        dodavanjeJeUspesno: false
    }
}