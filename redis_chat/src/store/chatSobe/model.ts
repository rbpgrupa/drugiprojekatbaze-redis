export enum AkcijeChatSobe
{
    UCITAJ_NAZIVE_SOBA = "[Akcije Chat Sobe] Ucitaj nazive soba",
    PROSLEDI_NAZIVE_REDUCERU = "[Akcije Chat Sobe] Prosledi nazive reduceru",
    UPISI_SOBU_U_BAZU = "[Akcije Chat Sobe] Upisi sobu u bazu",
    DODAVANJE_SOBE_USPEH = "[Akcije Chat Sobe] Dodavanje sobe uspeh",
    DODAVANJE_SOBE_GRESKA = "[Akcije Chat Sobe] Dodavanje sobe greska"
}

export interface ChatSoba
{
    naziv: string
}

//----> sad za akcije
export interface UcitajSobe
{
    type: AkcijeChatSobe
}

export interface ProslediReduceru
{
    type: AkcijeChatSobe,
    nazivi: string[]
}

export interface DodajNovuSobu
{
    type: AkcijeChatSobe,
    naziv: string
}

export interface DodavanjeSobeUspeh
{
    type: AkcijeChatSobe,
    dodavanjeJeUspesno: boolean,
    nazivSobe: string
}

export interface DodavanjeSobeNeuspeh
{
    type: AkcijeChatSobe,
    dodavanjeJeUspesno: boolean
}