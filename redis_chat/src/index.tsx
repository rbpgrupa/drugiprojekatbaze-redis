import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import konfigurisiStore from './store';
import { Provider } from 'react-redux';

//import * as serviceWorker from './serviceWorker'; bem ga, bice zatreba...

const store = konfigurisiStore();
//NEVEZANO ZA PORUKE JER TESTIRAM: dodacu i da reducer onaj za chatSobe ovo slusa, pa cu tu posle refactor...

//promenio sam nesto kako bi se valjda lepo menjale komponente
ReactDOM.render(<Provider store={store}>
                    <App/>
                </Provider>, document.getElementById('root'));